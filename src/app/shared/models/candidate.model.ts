import { EducationViewModel } from './education.mode';
import { ExperienceViewModel } from './experience.model';

export class Candidate {
  jobTitle: string;
  city: string;
  fullName: string;
  email: string;
  password: string;
  confirmPassword: string;
  resume?: string;
  experiences?: ExperienceViewModel[];
  educations?: EducationViewModel[];
  willToWorkRemotely: true;
  countryId: 0;
  minimumDesirePay: 0;
  isLookingActive: true;
  isProfileSearchable: true;
  platFormType: 0;
}
