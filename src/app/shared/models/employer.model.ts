export class Employer {
  fullName: string;
  email: string;
  password: string;
  confirmPassword: string;
  jobsNumber: 0;
  hiringNeed: 0;
  currentTitle: 0;
  phoneNumber: string;
  zipCode: string;
  trackingSystem: 0;
  marketingOption: 0;
  specifyMarketing: string;
  platFormType: 0;
}
