import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faSearchLocation } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  faLocation = faSearchLocation;
  isHome: boolean;
  isForgotPass: boolean;

  constructor(private router: Router) {}

  ngOnInit(): void {
    if (this.router.url === '/home') {
      this.isHome = true;
    } else {
      this.isHome = false;
    }

    if (this.router.url === '/forgot-password') {
      this.isForgotPass = true;
    } else {
      this.isForgotPass = false;
    }
  }
}
