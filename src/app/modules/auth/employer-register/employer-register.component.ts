import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-employer-register',
  templateUrl: './employer-register.component.html',
  styleUrls: ['./employer-register.component.css'],
})
export class EmployerRegisterComponent implements OnInit {
  employerForm: FormGroup;

  step = 1;

  constructor(private api: ApiService, private router: Router) {}

  ngOnInit(): void {
    this.employerForm = new FormGroup({
      fullName: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required),
      confirmPassword: new FormControl(null, Validators.required),
      jobsNumber: new FormControl(0, Validators.required),
      hiringNeed: new FormControl(0, Validators.required),
      currentTitle: new FormControl(0, Validators.required),
      phoneNumber: new FormControl(null, Validators.required),
      zipCode: new FormControl(null, Validators.required),
      trackingSystem: new FormControl(0, Validators.required),
      marketingOption: new FormControl(0, Validators.required),
    });
  }

  onNext() {
    this.step = this.step + 1;
  }

  onSubmit() {
    this.api.addNewEmployer(this.employerForm.value).subscribe(
      (res) => {
        alert(res.serverParams.Message);
        this.router.navigate(['/login/1']);
        console.log('SUCCESS: ', res);
      },
      (err) => console.log('ERROR: ', err)
    );
    console.log('employer data: ', this.employerForm.value);
  }
}
