import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './errors/page-not-found/page-not-found.component';
import { ActivateEmailComponent } from './modules/auth/activate-email/activate-email.component';
import { CandidateRegisterComponent } from './modules/auth/candidate-register/candidate-register.component';
import { ChangePasswordComponent } from './modules/auth/change-password/change-password.component';
import { EmployerRegisterComponent } from './modules/auth/employer-register/employer-register.component';
import { ForgotPasswordComponent } from './modules/auth/forgot-password/forgot-password.component';
import { LoginComponent } from './modules/auth/login/login.component';
import { HomeComponent } from './modules/home/home.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', component: HomeComponent },
  { path: 'login/:userType', component: LoginComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'change-password', component: ChangePasswordComponent },
  { path: 'activate-email', component: ActivateEmailComponent },
  { path: 'new-employer', component: EmployerRegisterComponent },
  { path: 'new-candidate', component: CandidateRegisterComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
