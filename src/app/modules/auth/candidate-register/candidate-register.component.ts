import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { faChevronRight, faPlus } from '@fortawesome/free-solid-svg-icons';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-candidate-register',
  templateUrl: './candidate-register.component.html',
  styleUrls: ['./candidate-register.component.css'],
})
export class CandidateRegisterComponent implements OnInit {
  candidateForm: FormGroup;

  step = 1;
  faPlus = faPlus;
  faChevron = faChevronRight;
  showMoreInfo = false;

  constructor(private api: ApiService, private router: Router) {}

  ngOnInit(): void {
    this.candidateForm = new FormGroup({
      jobTitle: new FormControl(null, Validators.required),
      city: new FormControl(null, Validators.required),
      fullName: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required),
      confirmPassword: new FormControl(null, Validators.required),
      isLookingActive: new FormControl(null, Validators.required),
      isProfileSearchable: new FormControl(null, Validators.required),
      resume: new FormControl(null),
      recentJobTitle: new FormControl(null),
      recentCompany: new FormControl(null),
      stillWorkThere: new FormControl(null),
      jobDescription: new FormControl(null),
      school: new FormControl(null, Validators.required),
      degree: new FormControl(null, Validators.required),
      schoolDescription: new FormControl(null),
    });
  }

  onNext() {
    this.step = this.step + 1;
  }

  onMoreInfo() {
    this.showMoreInfo = !this.showMoreInfo;
  }

  onSubmit() {
    this.candidateForm.patchValue({
      isLookingActive: !!this.candidateForm.value.isLookingActive,
    });
    this.api.addNewCandidate(this.candidateForm.value).subscribe(
      (res) => {
        alert(res.serverParams.Message);
        this.router.navigate(['/login/2']);
        console.log('SUCCESS: ', res);
      },
      (err) => console.log('ERROR: ', err)
    );
    console.log('candidate data: ', this.candidateForm.value);
  }
}
