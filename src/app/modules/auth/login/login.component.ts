import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  currentUser: number; // 1 is Employee, 2 is Candidate

  constructor(
    private route: ActivatedRoute,
    private api: ApiService,
    private router: Router
  ) {}

  onClick(userSelected: number) {
    this.currentUser = userSelected;
  }

  ngOnInit(): void {
    this.currentUser = +this.route.snapshot.paramMap.get('userType');

    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required),
    });
  }

  onSubmit() {
    this.api.userLogin(this.loginForm.value).subscribe(
      (res) => {
        if(res.errorMessage !== null){
          alert(res.errorMessage);
        } else {
          alert("Logged In Successfully!");
        }
        console.log('SUCCESS: ', res);
      },
      (err) => console.log('ERROR: ', err)
    );
    console.log('login data: ', this.loginForm.value);
  }
}
