import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
})
export class ForgotPasswordComponent implements OnInit {
  forgotForm: FormGroup;

  faChevron = faChevronLeft;

  constructor(private api: ApiService) {}

  ngOnInit(): void {
    this.forgotForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
    });
  }

  onSubmit() {
    this.api.userForgotPassword(this.forgotForm.value).subscribe(
      (res) => {
        if (res.errorMessage !== null) {
          alert(res.errorMessage);
        } else {
          alert('Check your email for new password!');
        }
        console.log('SUCCESS: ', res);
      },
      (err) => console.log('ERROR: ', err)
    );
    console.log('password data: ', this.forgotForm.value);
  }
}
