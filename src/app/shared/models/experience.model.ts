export class ExperienceViewModel {
  companyName: string;
  city?: string;
  country?: string;
  title: string;
  dateFrom: '2021-12-05T04:42:58.551Z';
  dateTo: '2021-12-05T04:42:58.551Z';
  stillWorkThere: true;
  description?: string;
}
