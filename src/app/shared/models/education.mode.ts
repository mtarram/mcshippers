export class EducationViewModel {
  school: string;
  dateFrom: '2021-12-05T04:42:58.551Z';
  dateTo: '2021-12-05T04:42:58.551Z';
  degree: string;
  areaOfStudy?: string;
  description: string;
}
