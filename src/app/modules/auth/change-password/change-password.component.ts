import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css'],
})
export class ChangePasswordComponent implements OnInit {
  changePasswordForm: FormGroup;

  faChevron = faChevronLeft;

  constructor(private api: ApiService) {}

  ngOnInit(): void {
    this.changePasswordForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      currentPassword: new FormControl(null, Validators.required),
      newPassword: new FormControl(null, Validators.required),
    });
  }

  onSubmit() {
    this.api.userChangePassword(this.changePasswordForm.value).subscribe(
      (res) => {
        if (res.errorMessage !== null) {
          alert(res.errorMessage);
        } else {
          alert('Password Changed Successfully!');
        }
        console.log('SUCCESS: ', res);
      },
      (err) => alert(err.statusMessage)
    );
    console.log('changed password data: ', this.changePasswordForm.value);
  }
}
