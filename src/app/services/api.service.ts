import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Candidate } from '../shared/models/candidate.model';
import { Employer } from '../shared/models/employer.model';
import { User } from '../shared/models/user.model';

const BASE_URL = 'https://hiring2021.mcshippers.com';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  // CANDIDATE REGISTER
  addNewCandidate(candidateData: Candidate): Observable<any> {
    return this.http.post<Candidate>(
      `${BASE_URL}/api/Users/CandidateRegister`,
      candidateData
    );
  }

  // EMPLOYER REGISTER
  addNewEmployer(employerData: Employer): Observable<any> {
    return this.http.post<Employer>(
      `${BASE_URL}/api/Users/EmployerRegister`,
      employerData
    );
  }

  // USER LOGIN
  userLogin(user: User): Observable<any> {
    return this.http.post<User>(`${BASE_URL}/api/Account/Login`, user);
  }

  // USER CHANGE PASSWORD
  userChangePassword(userData: any): Observable<any> {
    return this.http.post<any>(
      `${BASE_URL}/api/Account/ChangePassword`,
      userData
    );
  }

  // USER FORGOT PASSWORD
  userForgotPassword(userData: any): Observable<any> {
    return this.http.post<any>(
      `${BASE_URL}/api/Account/ForgetPassword`,
      userData
    );
  }

  // USER ACTIVATE EMAIL
  userActivateEmail(userData: any): Observable<any> {
    return this.http.post<any>(
      `${BASE_URL}/api/Account/ResendActivationEmail`,
      userData
    );
  }
}
