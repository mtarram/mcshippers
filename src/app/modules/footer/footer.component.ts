import { Component, OnInit } from '@angular/core';
import { faFacebook, faInstagram, faLinkedin, faTwitterSquare } from '@fortawesome/free-brands-svg-icons';
import { faChevronRight, faPlus } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  faFacebook = faFacebook;
  faTwitter = faTwitterSquare;
  faLinkedin = faLinkedin;
  faInstagram = faInstagram;
  faPlus = faPlus;
  faChevron = faChevronRight;

  constructor() { }

  ngOnInit(): void {
  }

}
