import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-activate-email',
  templateUrl: './activate-email.component.html',
  styleUrls: ['./activate-email.component.css'],
})
export class ActivateEmailComponent implements OnInit {
  activateForm: FormGroup;

  faChevron = faChevronLeft;

  constructor(private api: ApiService) {}

  ngOnInit(): void {
    this.activateForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
    });
  }

  onSubmit() {
    this.api.userActivateEmail(this.activateForm.value).subscribe(
      (res) => {
        if (res.errorMessage !== null) {
          alert(res.errorMessage);
        } else {
          alert('Check your email activation!');
        }
        console.log('SUCCESS: ', res);
      },
      (err) => console.log('ERROR: ', err)
    );
    console.log('activation data: ', this.activateForm.value);
  }
}
